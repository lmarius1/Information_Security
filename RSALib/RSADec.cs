﻿using ConversionsLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RSALib
{
    public class RSADec
    {
        private Conversions conv = Conversions.getInstance();
        private RSAParameters PrivKey;
        private RSACryptoServiceProvider CSP;
        public RSADec(string privKeyString)
        {
            SetPrivKey(privKeyString);
            CSP = new RSACryptoServiceProvider();
            CSP.ImportParameters(PrivKey);
        }
        public string Decrypt(string hexCipherText, bool decryptToHexadecimal)
        {
            //hexCipherText is in hexadecimal form.
            //Will return decrypted plain text in ASCII form.
            var bytesCipherText = conv.HexStringToByteArray(hexCipherText);
            //decrypt and strip pkcs#1.5 padding
            var bytesPlainTextData = CSP.Decrypt(bytesCipherText, true);
            //return ByteArrayToHexString(bytesPlainTextData);
            return decryptToHexadecimal ? conv.ByteArrayToHexString(bytesPlainTextData) : 
                Encoding.Unicode.GetString(bytesPlainTextData);
        }
        private void SetPrivKey(string privKeyString)
        {
            //get a stream from the string
            var sr = new System.IO.StringReader(privKeyString);
            //we need a deserializer
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
            //get the object back from the stream
            PrivKey = (RSAParameters)xs.Deserialize(sr);
        }
    }
}
