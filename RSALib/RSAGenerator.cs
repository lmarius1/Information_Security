﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RSALib
{
    public class RSAGenerator
    {
        public string PubKeyString { get; private set; }
        public string PrivKeyString { get; private set; }
        public RSAParameters PrivKey { get; private set; }
        public RSAParameters PubKey { get; private set; }
        public RSACryptoServiceProvider csp { get; private set; }
        public RSAGenerator(int length)
        {
            csp = new RSACryptoServiceProvider(length);

            //how to get the private key
            PrivKey = csp.ExportParameters(true);

            //and the public key ...
            PubKey = csp.ExportParameters(false);

            //setting public and private strings
            SetPrivKeyString();
            SetPubKeyString();
        }

        //Both public and private RSAParameters are converted into XML format.
        //Then they can be saved in a file
        private void SetPubKeyString()
        {
            //we need some buffer
            var sw = new System.IO.StringWriter();
            //we need a serializer
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
            //serialize the key into the stream
            xs.Serialize(sw, PubKey);
            //get the string from the stream
            PubKeyString = sw.ToString();
        }
        private void SetPrivKeyString()
        {
            //we need some buffer
            var sw = new System.IO.StringWriter();
            //we need a serializer
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
            //serialize the key into the stream
            xs.Serialize(sw, PrivKey);
            //get the string from the stream
            PrivKeyString = sw.ToString();
        }
    }
}
