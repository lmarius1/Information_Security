﻿using ConversionsLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RSALib
{
    public class RSAEnc
    {
        private Conversions conv = Conversions.getInstance();
        private RSAParameters PubKey;
        private RSACryptoServiceProvider CSP;

        public RSAEnc(string pubKeyString)
        {
            SetPubKey(pubKeyString);

            //create an instance of RSACryptoservice provider and import public key parameters
            CSP = new RSACryptoServiceProvider();
            CSP.ImportParameters(PubKey);
        }
        private void SetPubKey(string pubKeyString)
        {
            //get a stream from the string
            var sr = new System.IO.StringReader(pubKeyString);
            //we need a deserializer
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
            //get the object back from the stream
            PubKey = (RSAParameters)xs.Deserialize(sr);
        }

        /// <summary>
        /// Returns ciphertext in hexadecimal string form.
        /// </summary>
        /// <param name="pt"></param>
        /// <param name="ptIsInHexadecimalFormat"></param>
        /// <returns></returns>
        public string Encrypt(string pt, bool ptIsInHexadecimalFormat)
        {
            byte[] bytesPlainTextData;
            if (!ptIsInHexadecimalFormat)
                bytesPlainTextData = Encoding.Unicode.GetBytes(pt);
            else
                bytesPlainTextData = conv.HexStringToByteArray(pt);

            var bytesCipherText = CSP.Encrypt(bytesPlainTextData, true);
            return conv.ByteArrayToHexString(bytesCipherText);
        }
        public string Encrypt(byte[] bytesPlainTextData)
        {
            var bytesCipherText = CSP.Encrypt(bytesPlainTextData, true);
            return conv.ByteArrayToHexString(bytesCipherText);
        }
    }
}
