﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AESLib
{
    public class AESC
    {
        public AesManaged aesAlg { get; private set; }
        public AESC()
        {
            aesAlg = new AesManaged();
            aesAlg.GenerateKey();
        }

        public AESC(string[] parameters)
        {
            aesAlg = new AesManaged();
            importAESParameters(parameters);
        }

        public void Encrypt(FileStream fin, FileStream fout)
        {
            aesAlg.GenerateIV();
            fout.Write(aesAlg.IV, 0, aesAlg.IV.Length);
            //aesAlg.Mode = cipherMode;

            //Create variables to help with read and write.
            byte[] bin = new byte[512]; //This is intermediate storage for the encryption.
            long rdlen = 0;              //This is the total number of bytes written.
            long totlen = fin.Length;    //This is the total length of the input file.
            int len;                     //This is the number of bytes to be written at a time.

            using (CryptoStream encStream = new CryptoStream(fout, aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV), CryptoStreamMode.Write))
            {

                //Console.WriteLine("Encrypting...");

                //Read from the input file, then encrypt and write to the output file.
                while (rdlen < totlen)
                {
                    len = fin.Read(bin, 0, 512);
                    encStream.Write(bin, 0, len);
                    rdlen = rdlen + len;
                    //Console.WriteLine("{0} bytes processed", rdlen);
                }

                //encStream.FlushFinalBlock();
            }
        }
        public void Decrypt(FileStream fin, FileStream fout, byte[] key, int offset)
        {
            //offset determines how many bytes were either already read from fin
            //OR (as in this case) how many bytes will be read from the fin before starting decryption.
            //In this case first 16 bytes will determine IV (which is not encrypted)
            byte[] IV = new byte[offset];
            fin.Read(IV, 0, offset); //read IV from files. Should be first 16 bytes.
            aesAlg.IV = IV;
            aesAlg.Key = key;

            //Create variables to help with read and write.
            byte[] bin = new byte[512]; //This is intermediate storage for the encryption.
            long rdlen = offset;              //This is the total number of bytes written.
            long totlen = fin.Length;    //This is the total length of the input file.
            int len;                     //This is the number of bytes to be written at a time.

            using (CryptoStream decStream = new CryptoStream(fout, aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV), CryptoStreamMode.Write))
            {

                //Read from the input file, then encrypt and write to the output file.
                while (rdlen < totlen)
                {
                    len = fin.Read(bin, 0, 512);
                    decStream.Write(bin, 0, len);
                    rdlen = rdlen + len;
                    //Console.WriteLine("{0} bytes processed", rdlen);
                }

                //decStream.FlushFinalBlock();
            }
        }
        public string exportAESParameters()
        {
            StringBuilder sb = new StringBuilder();

            //define that the cipher used is AES
            sb.Append("AES" + Environment.NewLine);

            //Define all the other parameters of the cipher
            sb.Append(aesAlg.BlockSize + Environment.NewLine);
            sb.Append(aesAlg.FeedbackSize + Environment.NewLine);
            sb.Append(aesAlg.KeySize + Environment.NewLine);
            sb.Append(aesAlg.Mode + Environment.NewLine);
            sb.Append(aesAlg.Padding);

            return sb.ToString();
        }

        public void importAESParameters(string[] aesParameters)
        {
            if (aesParameters.Length != 6)
            {
                throw new ArgumentException("Array length should be 6, in order to import these parameters correctly");
            }
            if (aesParameters[0] != "AES")
            {
                throw new NotSupportedException("Algorithm type is not AES. Cannot import parameters.");
            }
            aesAlg.BlockSize = int.Parse(aesParameters[1]);
            aesAlg.FeedbackSize = int.Parse(aesParameters[2]);
            aesAlg.KeySize = int.Parse(aesParameters[3]);

            aesAlg.Mode = getCipherMode(aesParameters[4]);

            switch (aesParameters[5])
            {
                case "PKCS7":
                    aesAlg.Padding = PaddingMode.PKCS7;
                    break;
                case "ANSIX923":
                    aesAlg.Padding = PaddingMode.ANSIX923;
                    break;
                case "ISO10126":
                    aesAlg.Padding = PaddingMode.ISO10126;
                    break;
                case "None":
                    aesAlg.Padding = PaddingMode.None;
                    break;
                case "Zeros":
                    aesAlg.Padding = PaddingMode.Zeros;
                    break;
                default:
                    throw new ArgumentException("Could not recognize padding mode at line 6");
            }
        }
        private CipherMode getCipherMode(String strCipherMode)
        {
            switch (strCipherMode)
            {
                case "CBC":
                    return CipherMode.CBC;
                case "CFB":
                    return CipherMode.CFB;
                case "CTS":
                    return CipherMode.CTS;
                case "ECB":
                    return CipherMode.ECB;
                case "OFB":
                    return CipherMode.OFB;
                default:
                    throw new ArgumentException("Could not recognize cipher mode at line 5");
            }
        }
    }
}
