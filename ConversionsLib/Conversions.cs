﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversionsLib
{
    public class Conversions
    {
        private static Conversions _conversions = new Conversions();
        private Conversions() { }
        public static Conversions getInstance()
        {
            return _conversions;
        }

        public byte[] StringToByteArray(string str)
        {
            return Encoding.UTF8.GetBytes(str);
        }
        public byte[] HexStringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
        public string ByteArrayToHexString(byte[] arr)
        {
            StringBuilder str = new StringBuilder();
            foreach (byte b in arr)
            {
                str.Append(b.ToString("x2"));
            }
            return str.ToString();
        }
        public string ByteArrayToASCIIText(byte[] arr)
        {
            return Encoding.UTF8.GetString(arr);
        }
        public byte[] XOR(byte[] arr1, byte[] arr2)
        {
            if (arr1.Length != arr2.Length)
                throw new ArgumentException("Byte arrays are not of the same length.");
            byte[] newArr = new byte[arr1.Length];
            arr1.CopyTo(newArr, 0);
            for (int i = 0; i < arr1.Length; i++)
            {
                newArr[i] ^= arr2[i];
            }
            return newArr;
        }
        public void Swap<T>(T[] s, int i, int j)
        {
            T tmp = s[i];
            s[i] = s[j];
            s[j] = tmp;
        }
    }
}
