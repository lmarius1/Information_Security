﻿using AESLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Information_Security_Test
{
    [TestClass]
    public class AESLibTest
    {
        [TestMethod]
        public void ShouldEncryptAndThenDecryptCorrectlyUsingAESWithCBCBlockModeAndPKCS7Padding()
        {
            string plainText = "My name is Marius and I like apples.";
            string ptFile = "TempFileForPlainText";
            string ctFile = "TempFileForCipherText";
            string decryptedFile = "";

            try
            {
                //Write plaintext to the file because AESC only encrypts and decrypts using filestreams.
                File.WriteAllText(ptFile, plainText);

                AESC aes = new AESC();

                aes.aesAlg.BlockSize = 128;
                aes.aesAlg.KeySize = 256;
                aes.aesAlg.Mode = System.Security.Cryptography.CipherMode.CBC;
                aes.aesAlg.Padding = System.Security.Cryptography.PaddingMode.PKCS7;
                aes.aesAlg.GenerateKey();
                byte[] aesKey = aes.aesAlg.Key;


                using (FileStream Fin = new FileStream(ptFile, FileMode.Open, FileAccess.Read))
                using (FileStream Fout = new FileStream(ctFile, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    aes.Encrypt(Fin, Fout);
                }

                string exportParametersString = aes.exportAESParameters();
                string[] exportParameters = exportParametersString.Split
                    (new[] { Environment.NewLine }, StringSplitOptions.None);

                //Check here if parameters were exported correctly. If not - throw an exception
                if (exportParameters[1] != "128")
                    throw new ArgumentException("Wrong block size exported");
                if (exportParameters[3] != "256")
                    throw new ArgumentException("Wrong key size exported");
                if (exportParameters[4] != "CBC")
                    throw new ArgumentException("Wrong Cipher Mode exported");
                if (exportParameters[5] != "PKCS7")
                    throw new ArgumentException("Wrong Padding Mode exported");


                //Decrypt the encrypted cipherText
                AESC aesForDecryption = new AESC(exportParameters);

                decryptedFile = "TempFileForDecryption";
                using (FileStream Fin = new FileStream(ctFile, FileMode.Open, FileAccess.Read))
                using (FileStream Fout = new FileStream(decryptedFile, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    aesForDecryption.Decrypt(Fin, Fout, aesKey, 16);    //offset is 16 because first 16 bytes are for IV (used both in encryption and decryption)
                }

                string decryptedText = File.ReadAllText(decryptedFile);

                Assert.AreEqual(plainText, decryptedText);
            }
            finally
            {
                if (File.Exists(ptFile))
                    File.Delete(ptFile);

                if (File.Exists(ctFile))
                    File.Delete(ctFile);

                if (File.Exists(decryptedFile))
                    File.Delete(decryptedFile);
            }
        }
    }
}
