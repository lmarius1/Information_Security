﻿using ConversionsLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RC4Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Information_Security_Test
{
    [TestClass]
    public class RC4LibTest
    {
        RC4 rc4 = new RC4();
        Conversions conv = Conversions.getInstance();

        [TestMethod]
        //Expected answer was calculated here: http://rc4.online-domain-tools.com/
        public void ShouldEncryptRC4MessageAsExpected()
        {
            string key = "SomeKeyHere.";
            string plainText = "A very secret message.";
            string expectedCipherTextHex = "527e07730686926416053a6e6b8b34dd54de60faa29d";
            
            string cipherTextHex = conv.ByteArrayToHexString(rc4.Encrypt(key, plainText));
            Assert.AreEqual(expectedCipherTextHex, cipherTextHex);
        }

        [TestMethod]
        public void ShouldDecryptRC4Correctly()
        {
            string key = "SomeKeyHere.";
            string cipherTextHex = "527e07730686926416053a6e6b8b34dd54de60faa29d";

            string expectedPlainText = "A very secret message.";

            string calculatedPlainText = conv.ByteArrayToASCIIText(
                rc4.Decrypt(conv.StringToByteArray(key), 
                conv.HexStringToByteArray(cipherTextHex)));
            Assert.AreEqual(expectedPlainText, calculatedPlainText);
        }

        [TestMethod]
        public void ShouldObtainUnknownPlainTextByKnownPlainTextKeyAttackWhenFirstMessageIsLonger()
        {
            string key = "SomeKeyHere.";    //a key with which two messages will be encrypted.
            string knownPlainText = "A very secret message.";
            string knownCipherTextHex = conv.ByteArrayToHexString(rc4.Encrypt(key, knownPlainText));

            //Encrypting another message with the same key.
            string unknownPlainText = "Marius";
            string knownCipherTextHexOfUnknownPlaintext = conv.ByteArrayToHexString(
                rc4.Encrypt(key, unknownPlainText));

            string obtainedPlainText = conv.ByteArrayToASCIIText(
                rc4.DecryptOtherPlaintext(conv.StringToByteArray(knownPlainText),
                conv.HexStringToByteArray(knownCipherTextHex), 
                conv.HexStringToByteArray(knownCipherTextHexOfUnknownPlaintext)));

            Assert.AreEqual(unknownPlainText, obtainedPlainText);
        }

        [TestMethod]
        public void ShouldObtainUnknownPlainTextByKnownPlainTextKeyAttackWhenBothPlaintextsAreSameLength()
        {
            string key = "SomeKeyHere.";    //a key with which two messages will be encrypted.
            string knownPlainText = "A very secret message.";
            string knownCipherTextHex = conv.ByteArrayToHexString(rc4.Encrypt(key, knownPlainText));

            //Encrypting another message with the same key.
            string unknownPlainText = "SameLengthMessageHere.";
            string knownCipherTextHexOfUnknownPlaintext = conv.ByteArrayToHexString(
                rc4.Encrypt(key, unknownPlainText));

            string obtainedPlainText = conv.ByteArrayToASCIIText(
                rc4.DecryptOtherPlaintext(conv.StringToByteArray(knownPlainText),
                conv.HexStringToByteArray(knownCipherTextHex),
                conv.HexStringToByteArray(knownCipherTextHexOfUnknownPlaintext)));

            Assert.AreEqual(unknownPlainText, obtainedPlainText);
        }

        [TestMethod]
        public void ShouldObtainUnknownPartOfPlainTextByKnownPlainTextKeyAttackWhenUnknownMessageIsLonger()
        {
            string key = "SomeKeyHere.";    //a key with which two messages will be encrypted.
            string knownPlainText = "A very secret message.";
            string knownCipherTextHex = conv.ByteArrayToHexString(rc4.Encrypt(key, knownPlainText));

            //Encrypting another message with the same key.
            string unknownPlainText = "This is a longer text part of which should be decrypted.";
            string unknownPlainTextPart = "This is a longer text ";
            string knownCipherTextHexOfUnknownPlaintext = conv.ByteArrayToHexString(
                rc4.Encrypt(key, unknownPlainText));

            string obtainedPlainText = conv.ByteArrayToASCIIText(
                rc4.DecryptOtherPlaintext(conv.StringToByteArray(knownPlainText),
                conv.HexStringToByteArray(knownCipherTextHex),
                conv.HexStringToByteArray(knownCipherTextHexOfUnknownPlaintext)));

            Assert.AreEqual(unknownPlainTextPart, obtainedPlainText);
        }
    }
}
