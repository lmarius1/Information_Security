﻿using ConversionsLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSALib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Information_Security_Test
{
    [TestClass]
    public class RSALibTest
    {

        [TestMethod]
        public void ShouldGenerateRSAOfTheCorrectLength()
        {
            int length = 4096;
            RSAGenerator rsaGenerator = new RSAGenerator(length);
            Assert.AreEqual(length, rsaGenerator.csp.KeySize);
        }

        [TestMethod]
        public void ShouldEncryptAndThenDecryptCorrectlyUsingRSA()
        {
            string plainText = "Some secret message.";

            //Generate RSA parameters
            int length = 2048;
            RSAGenerator rsaGen = new RSAGenerator(length);
            string pubKey = rsaGen.PubKeyString;
            string privKey = rsaGen.PrivKeyString;

            //Encrypt plaintext with generated public parameters
            RSAEnc rsaEncrypt = new RSAEnc(pubKey);
            string cipherTextHex = rsaEncrypt.Encrypt(plainText, false);

            //Decrypt ciphertext with generated private parameters
            RSADec rsaDecrypt = new RSADec(privKey);
            string calculatedPlainText = rsaDecrypt.Decrypt(cipherTextHex, false);

            Assert.AreEqual(plainText, calculatedPlainText);
        }
    }
}
