﻿using ConversionsLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Information_Security_Test
{
    [TestClass]
    public class ConversionsLibTest
    {
        Conversions conv = Conversions.getInstance();

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Byte arrays are not of the same length.")]
        public void ShouldGiveAnErrorWhenXORingUnequalArrays()
        {
            byte[] arr1 = new byte[1];
            byte[] arr2 = new byte[2];
            conv.XOR(arr1, arr2);
        }

        [TestMethod]
        public void XORingTwoEmptyArraysShouldReturnAnEmptyArray()
        {
            byte[] arr1 = new byte[0];
            byte[] arr2 = new byte[0];
            byte[] arr3 = new byte[0];
            Assert.AreEqual(0, (conv.XOR(arr1, arr2)).Length);
        }

        [TestMethod]
        public void ShouldXORCorrectlyASampleTestWhenXORingSameValues()
        {
            byte[] arr1 = { 3, 10, 8, 25 };
            byte[] arr2 = { 3, 10, 8, 25 };
            byte[] expectedAnswer = { 0, 0, 0, 0 };
            Assert.IsTrue(
                expectedAnswer.Cast<byte>().SequenceEqual(
                (conv.XOR(arr1, arr2)).Cast<byte>()));
            
        }

        [TestMethod]
        public void ShouldXORCorrectlyASampleTestWhenXORingDifferentValues()
        {
            byte[] arr1 = { 3, 10, 8, 255 };
            byte[] arr2 = { 7, 4, 127, 255 };
            byte[] expectedAnswer = { 4, 14, 119, 0 };

            Assert.IsTrue(
                expectedAnswer.Cast<byte>().SequenceEqual(
                (conv.XOR(arr1, arr2)).Cast<byte>()));
        }

        [TestMethod]
        public void XORingShouldReturnFalseWithWrongExpectedAnswer()
        {
            byte[] arr1 = { 3, 10, 8, 255 };
            byte[] arr2 = { 7, 4, 127, 255 };
            byte[] expectedAnswer = { 4, 14, 119, 1 };

            Assert.IsFalse(
                expectedAnswer.Cast<byte>().SequenceEqual(
                (conv.XOR(arr1, arr2)).Cast<byte>()));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ShouldGiveAnErrorWhenHexStringIsNotDivisibleByTwo()
        {
            string hex = "ebc";
            conv.HexStringToByteArray(hex);
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void ShouldGiveAnErrorWhenNonHexValueIsInString()
        {
            string hex = "ebct";
            conv.HexStringToByteArray(hex);
        }

        [TestMethod]
        public void ShouldConvertCorrectlyHexStringToByteArray()
        {
            string hex = "fec1";
            byte[] expectedAnswer = { 254, 193 };
            byte[] calculatedAnswer = conv.HexStringToByteArray(hex);

            Assert.IsTrue(expectedAnswer.Cast<byte>().SequenceEqual(
                calculatedAnswer));
        }

        [TestMethod]
        public void ShouldReturnFalseWithWrongExpectedAnswerWhenConvertingHexStringToByteArray()
        {
            string hex = "fec1";
            byte[] expectedAnswer = { 254, 0 };
            byte[] calculatedAnswer = conv.HexStringToByteArray(hex);

            Assert.IsFalse(expectedAnswer.Cast<byte>().SequenceEqual(
                calculatedAnswer));
        }
    }
}
