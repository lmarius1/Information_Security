﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RabinMiller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Information_Security_Test
{
    [TestClass]
    public class RabinMillerTest_UnitTesting
    {
        RabinMillerTest rabinMiller = new RabinMillerTest();

        [TestMethod]
        //7 should be a strong liar that 25 is prime.
        public void ShouldCorrectlyCalculate25Base7RabinMillerTest()
        {
            BigInteger probablePrime = BigInteger.Parse("25");
            BigInteger a = BigInteger.Parse("7");

            bool isProbablyPrime = rabinMiller.IsProbablePrime(probablePrime, 0, a);
            Assert.IsTrue(isProbablyPrime); //7 should tell that 25 is probably prime.
        }

        [TestMethod]
        public void ShouldCorrectlyCalculateThat25IsCertainlyNotAPrimeUsingBase2RabinMillerTest()
        {
            BigInteger probablePrime = BigInteger.Parse("25");
            BigInteger a = BigInteger.Parse("2");    //2 is a strong witness that 25 is not a prime.

            bool isProbablyPrime = rabinMiller.IsProbablePrime(probablePrime, 0, a);
            Assert.IsFalse(isProbablyPrime);
        }
    }
}
