﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Information_Security
{
    public partial class FrmWaitForm : Form
    {
        public Action Worker { get; set; }


        public FrmWaitForm(Action worker)
        {
            Worker = worker ?? throw new ArgumentNullException();

            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Task.Factory.StartNew(Worker).ContinueWith(t => { this.Close(); },
                TaskScheduler.FromCurrentSynchronizationContext());
            //Task t = new Task(Worker);
            //t.Start();
            //t.Wait();
        }
    }
}
