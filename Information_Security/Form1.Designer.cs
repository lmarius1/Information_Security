﻿namespace Information_Security
{
    partial class Information_Security_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tcAll = new System.Windows.Forms.TabControl();
            this.tbRC4 = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbKPAUnknownPT = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnCalculateUnknownPlaintext = new System.Windows.Forms.Button();
            this.tbKPAKnownCTofUnknownPT = new System.Windows.Forms.TextBox();
            this.tbKPAKnownCTofKnownPT = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbKPAKnownPT = new System.Windows.Forms.TextBox();
            this.btnDecryptRC4 = new System.Windows.Forms.Button();
            this.btnEncryptRC4 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbKeyRC4 = new System.Windows.Forms.TextBox();
            this.tbRC4Ciphertext = new System.Windows.Forms.TextBox();
            this.tbRC4Plaintext = new System.Windows.Forms.TextBox();
            this.tbRSA = new System.Windows.Forms.TabPage();
            this.cbDecryptToHexadecimal = new System.Windows.Forms.CheckBox();
            this.cbIsHexadecimal = new System.Windows.Forms.CheckBox();
            this.btnRSADecrypt = new System.Windows.Forms.Button();
            this.btnRSAEncrypt = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.tbRSAPrivateBrowse = new System.Windows.Forms.TextBox();
            this.btnRSAPrivateBrowse = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.tbRSAPublicBrowse = new System.Windows.Forms.TextBox();
            this.btnRSAPublicBrowse = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.tbRSACipherText = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbRSAPlaintext = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tbRSAOutputFileFolder = new System.Windows.Forms.TextBox();
            this.btnBrowseFolder = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnGenerateRSAParameters = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.tbPrivKeyFileNameOut = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbPubKeyFileNameOut = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbKeySize = new System.Windows.Forms.ComboBox();
            this.tbAES = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this.btnAESParametersBrowse = new System.Windows.Forms.Button();
            this.tbAESParametersIn = new System.Windows.Forms.TextBox();
            this.btnAESDecrypt = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.tbAESDecryptedFileName = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.btnAESDecryptFileBrowse = new System.Windows.Forms.Button();
            this.tbAESFileToDecrypt = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.tbAESParametersOut = new System.Windows.Forms.TextBox();
            this.btnAESEncrypt = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.tbAESEncryptedFileName = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.btnAESBrowseFileToEncrypt = new System.Windows.Forms.Button();
            this.tbAESFileToEncrypt = new System.Windows.Forms.TextBox();
            this.btnGenerateKey = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.tbAESKey = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.cbAESPadding = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cbAESMode = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.cbAESKeySize = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cbAESBlockSize = new System.Windows.Forms.ComboBox();
            this.tbRabinMiller = new System.Windows.Forms.TabPage();
            this.tbAnswers = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.nudA = new System.Windows.Forms.NumericUpDown();
            this.btnTestRabinMiller = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbRabinMillerProbablePrime = new System.Windows.Forms.TextBox();
            this.nudRabinMillerK = new System.Windows.Forms.NumericUpDown();
            this.label32 = new System.Windows.Forms.Label();
            this.tcAll.SuspendLayout();
            this.tbRC4.SuspendLayout();
            this.tbRSA.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tbAES.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tbRabinMiller.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRabinMillerK)).BeginInit();
            this.SuspendLayout();
            // 
            // tcAll
            // 
            this.tcAll.Controls.Add(this.tbRSA);
            this.tcAll.Controls.Add(this.tbAES);
            this.tcAll.Controls.Add(this.tbRC4);
            this.tcAll.Controls.Add(this.tbRabinMiller);
            this.tcAll.Location = new System.Drawing.Point(12, 12);
            this.tcAll.Name = "tcAll";
            this.tcAll.SelectedIndex = 0;
            this.tcAll.Size = new System.Drawing.Size(875, 609);
            this.tcAll.TabIndex = 0;
            // 
            // tbRC4
            // 
            this.tbRC4.Controls.Add(this.label32);
            this.tbRC4.Controls.Add(this.label12);
            this.tbRC4.Controls.Add(this.label11);
            this.tbRC4.Controls.Add(this.tbKPAUnknownPT);
            this.tbRC4.Controls.Add(this.label10);
            this.tbRC4.Controls.Add(this.btnCalculateUnknownPlaintext);
            this.tbRC4.Controls.Add(this.tbKPAKnownCTofUnknownPT);
            this.tbRC4.Controls.Add(this.tbKPAKnownCTofKnownPT);
            this.tbRC4.Controls.Add(this.label9);
            this.tbRC4.Controls.Add(this.label8);
            this.tbRC4.Controls.Add(this.label7);
            this.tbRC4.Controls.Add(this.tbKPAKnownPT);
            this.tbRC4.Controls.Add(this.btnDecryptRC4);
            this.tbRC4.Controls.Add(this.btnEncryptRC4);
            this.tbRC4.Controls.Add(this.label6);
            this.tbRC4.Controls.Add(this.label5);
            this.tbRC4.Controls.Add(this.label4);
            this.tbRC4.Controls.Add(this.tbKeyRC4);
            this.tbRC4.Controls.Add(this.tbRC4Ciphertext);
            this.tbRC4.Controls.Add(this.tbRC4Plaintext);
            this.tbRC4.Location = new System.Drawing.Point(4, 22);
            this.tbRC4.Name = "tbRC4";
            this.tbRC4.Padding = new System.Windows.Forms.Padding(3);
            this.tbRC4.Size = new System.Drawing.Size(867, 583);
            this.tbRC4.TabIndex = 1;
            this.tbRC4.Text = "RC4";
            this.tbRC4.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label12.Location = new System.Drawing.Point(174, 8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 20);
            this.label12.TabIndex = 30;
            this.label12.Text = "RC4";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label11.Location = new System.Drawing.Point(563, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(191, 20);
            this.label11.TabIndex = 29;
            this.label11.Text = "Known-plaintext attack";
            // 
            // tbKPAUnknownPT
            // 
            this.tbKPAUnknownPT.Location = new System.Drawing.Point(444, 470);
            this.tbKPAUnknownPT.Multiline = true;
            this.tbKPAUnknownPT.Name = "tbKPAUnknownPT";
            this.tbKPAUnknownPT.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbKPAUnknownPT.Size = new System.Drawing.Size(417, 96);
            this.tbKPAUnknownPT.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(444, 454);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(265, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "Unknown Plaintext obtained by Known-plaintext attack";
            // 
            // btnCalculateUnknownPlaintext
            // 
            this.btnCalculateUnknownPlaintext.Location = new System.Drawing.Point(447, 413);
            this.btnCalculateUnknownPlaintext.Name = "btnCalculateUnknownPlaintext";
            this.btnCalculateUnknownPlaintext.Size = new System.Drawing.Size(75, 23);
            this.btnCalculateUnknownPlaintext.TabIndex = 26;
            this.btnCalculateUnknownPlaintext.Text = "Calculate";
            this.btnCalculateUnknownPlaintext.UseVisualStyleBackColor = true;
            this.btnCalculateUnknownPlaintext.Click += new System.EventHandler(this.btnCalculateUnknownPlaintext_Click);
            // 
            // tbKPAKnownCTofUnknownPT
            // 
            this.tbKPAKnownCTofUnknownPT.Location = new System.Drawing.Point(444, 327);
            this.tbKPAKnownCTofUnknownPT.Multiline = true;
            this.tbKPAKnownCTofUnknownPT.Name = "tbKPAKnownCTofUnknownPT";
            this.tbKPAKnownCTofUnknownPT.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbKPAKnownCTofUnknownPT.Size = new System.Drawing.Size(417, 80);
            this.tbKPAKnownCTofUnknownPT.TabIndex = 25;
            // 
            // tbKPAKnownCTofKnownPT
            // 
            this.tbKPAKnownCTofKnownPT.Location = new System.Drawing.Point(444, 219);
            this.tbKPAKnownCTofKnownPT.Multiline = true;
            this.tbKPAKnownCTofKnownPT.Name = "tbKPAKnownCTofKnownPT";
            this.tbKPAKnownCTofKnownPT.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbKPAKnownCTofKnownPT.Size = new System.Drawing.Size(417, 80);
            this.tbKPAKnownCTofKnownPT.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(444, 311);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(289, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "Known Ciphertext of the unknown Plaintext (in hexadecimal)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(444, 200);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(277, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Known Ciphertext of the known Plaintext (in hexadecimal)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(444, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Known Plaintext";
            // 
            // tbKPAKnownPT
            // 
            this.tbKPAKnownPT.Location = new System.Drawing.Point(444, 101);
            this.tbKPAKnownPT.Multiline = true;
            this.tbKPAKnownPT.Name = "tbKPAKnownPT";
            this.tbKPAKnownPT.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbKPAKnownPT.Size = new System.Drawing.Size(417, 80);
            this.tbKPAKnownPT.TabIndex = 18;
            // 
            // btnDecryptRC4
            // 
            this.btnDecryptRC4.Location = new System.Drawing.Point(354, 419);
            this.btnDecryptRC4.Name = "btnDecryptRC4";
            this.btnDecryptRC4.Size = new System.Drawing.Size(75, 23);
            this.btnDecryptRC4.TabIndex = 16;
            this.btnDecryptRC4.Text = "Decrypt";
            this.btnDecryptRC4.UseVisualStyleBackColor = true;
            this.btnDecryptRC4.Click += new System.EventHandler(this.btnDecryptRC4_Click);
            // 
            // btnEncryptRC4
            // 
            this.btnEncryptRC4.Location = new System.Drawing.Point(354, 240);
            this.btnEncryptRC4.Name = "btnEncryptRC4";
            this.btnEncryptRC4.Size = new System.Drawing.Size(75, 23);
            this.btnEncryptRC4.TabIndex = 15;
            this.btnEncryptRC4.Text = "Encrypt";
            this.btnEncryptRC4.UseVisualStyleBackColor = true;
            this.btnEncryptRC4.Click += new System.EventHandler(this.btnEncryptRC4_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 261);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(136, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Ciphertext (in hexadecimal):";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Plaintext:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Key:";
            // 
            // tbKeyRC4
            // 
            this.tbKeyRC4.Location = new System.Drawing.Point(47, 53);
            this.tbKeyRC4.Name = "tbKeyRC4";
            this.tbKeyRC4.Size = new System.Drawing.Size(382, 20);
            this.tbKeyRC4.TabIndex = 11;
            // 
            // tbRC4Ciphertext
            // 
            this.tbRC4Ciphertext.Location = new System.Drawing.Point(12, 280);
            this.tbRC4Ciphertext.Multiline = true;
            this.tbRC4Ciphertext.Name = "tbRC4Ciphertext";
            this.tbRC4Ciphertext.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbRC4Ciphertext.Size = new System.Drawing.Size(417, 133);
            this.tbRC4Ciphertext.TabIndex = 10;
            // 
            // tbRC4Plaintext
            // 
            this.tbRC4Plaintext.Location = new System.Drawing.Point(12, 101);
            this.tbRC4Plaintext.Multiline = true;
            this.tbRC4Plaintext.Name = "tbRC4Plaintext";
            this.tbRC4Plaintext.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbRC4Plaintext.Size = new System.Drawing.Size(417, 133);
            this.tbRC4Plaintext.TabIndex = 9;
            // 
            // tbRSA
            // 
            this.tbRSA.Controls.Add(this.cbDecryptToHexadecimal);
            this.tbRSA.Controls.Add(this.cbIsHexadecimal);
            this.tbRSA.Controls.Add(this.btnRSADecrypt);
            this.tbRSA.Controls.Add(this.btnRSAEncrypt);
            this.tbRSA.Controls.Add(this.label20);
            this.tbRSA.Controls.Add(this.tbRSAPrivateBrowse);
            this.tbRSA.Controls.Add(this.btnRSAPrivateBrowse);
            this.tbRSA.Controls.Add(this.label19);
            this.tbRSA.Controls.Add(this.tbRSAPublicBrowse);
            this.tbRSA.Controls.Add(this.btnRSAPublicBrowse);
            this.tbRSA.Controls.Add(this.label18);
            this.tbRSA.Controls.Add(this.tbRSACipherText);
            this.tbRSA.Controls.Add(this.label17);
            this.tbRSA.Controls.Add(this.tbRSAPlaintext);
            this.tbRSA.Controls.Add(this.label16);
            this.tbRSA.Controls.Add(this.tbRSAOutputFileFolder);
            this.tbRSA.Controls.Add(this.btnBrowseFolder);
            this.tbRSA.Controls.Add(this.groupBox1);
            this.tbRSA.Location = new System.Drawing.Point(4, 22);
            this.tbRSA.Name = "tbRSA";
            this.tbRSA.Padding = new System.Windows.Forms.Padding(3);
            this.tbRSA.Size = new System.Drawing.Size(867, 583);
            this.tbRSA.TabIndex = 2;
            this.tbRSA.Text = "RSA";
            this.tbRSA.UseVisualStyleBackColor = true;
            // 
            // cbDecryptToHexadecimal
            // 
            this.cbDecryptToHexadecimal.AutoSize = true;
            this.cbDecryptToHexadecimal.Location = new System.Drawing.Point(431, 415);
            this.cbDecryptToHexadecimal.Name = "cbDecryptToHexadecimal";
            this.cbDecryptToHexadecimal.Size = new System.Drawing.Size(169, 17);
            this.cbDecryptToHexadecimal.TabIndex = 27;
            this.cbDecryptToHexadecimal.Text = "Decrypt to hexadecimal format";
            this.cbDecryptToHexadecimal.UseVisualStyleBackColor = true;
            // 
            // cbIsHexadecimal
            // 
            this.cbIsHexadecimal.AutoSize = true;
            this.cbIsHexadecimal.Location = new System.Drawing.Point(431, 247);
            this.cbIsHexadecimal.Name = "cbIsHexadecimal";
            this.cbIsHexadecimal.Size = new System.Drawing.Size(181, 17);
            this.cbIsHexadecimal.TabIndex = 26;
            this.cbIsHexadecimal.Text = "Plaintext is in hexadecimal format";
            this.cbIsHexadecimal.UseVisualStyleBackColor = true;
            // 
            // btnRSADecrypt
            // 
            this.btnRSADecrypt.Location = new System.Drawing.Point(435, 445);
            this.btnRSADecrypt.Name = "btnRSADecrypt";
            this.btnRSADecrypt.Size = new System.Drawing.Size(75, 23);
            this.btnRSADecrypt.TabIndex = 25;
            this.btnRSADecrypt.Text = "Decrypt";
            this.btnRSADecrypt.UseVisualStyleBackColor = true;
            this.btnRSADecrypt.Click += new System.EventHandler(this.btnRSADecrypt_Click);
            // 
            // btnRSAEncrypt
            // 
            this.btnRSAEncrypt.Location = new System.Drawing.Point(435, 270);
            this.btnRSAEncrypt.Name = "btnRSAEncrypt";
            this.btnRSAEncrypt.Size = new System.Drawing.Size(75, 23);
            this.btnRSAEncrypt.TabIndex = 24;
            this.btnRSAEncrypt.Text = "Encrypt";
            this.btnRSAEncrypt.UseVisualStyleBackColor = true;
            this.btnRSAEncrypt.Click += new System.EventHandler(this.btnRSAEncrypt_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(432, 370);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(151, 13);
            this.label20.TabIndex = 23;
            this.label20.Text = "Import private RSA parameters";
            // 
            // tbRSAPrivateBrowse
            // 
            this.tbRSAPrivateBrowse.Location = new System.Drawing.Point(431, 389);
            this.tbRSAPrivateBrowse.Name = "tbRSAPrivateBrowse";
            this.tbRSAPrivateBrowse.Size = new System.Drawing.Size(350, 20);
            this.tbRSAPrivateBrowse.TabIndex = 21;
            // 
            // btnRSAPrivateBrowse
            // 
            this.btnRSAPrivateBrowse.Location = new System.Drawing.Point(787, 389);
            this.btnRSAPrivateBrowse.Name = "btnRSAPrivateBrowse";
            this.btnRSAPrivateBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnRSAPrivateBrowse.TabIndex = 22;
            this.btnRSAPrivateBrowse.Text = "Browse";
            this.btnRSAPrivateBrowse.UseVisualStyleBackColor = true;
            this.btnRSAPrivateBrowse.Click += new System.EventHandler(this.btnRSAPrivateBrowse_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(432, 202);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(147, 13);
            this.label19.TabIndex = 20;
            this.label19.Text = "Import public RSA parameters";
            // 
            // tbRSAPublicBrowse
            // 
            this.tbRSAPublicBrowse.Location = new System.Drawing.Point(431, 221);
            this.tbRSAPublicBrowse.Name = "tbRSAPublicBrowse";
            this.tbRSAPublicBrowse.Size = new System.Drawing.Size(350, 20);
            this.tbRSAPublicBrowse.TabIndex = 18;
            // 
            // btnRSAPublicBrowse
            // 
            this.btnRSAPublicBrowse.Location = new System.Drawing.Point(787, 221);
            this.btnRSAPublicBrowse.Name = "btnRSAPublicBrowse";
            this.btnRSAPublicBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnRSAPublicBrowse.TabIndex = 19;
            this.btnRSAPublicBrowse.Text = "Browse";
            this.btnRSAPublicBrowse.UseVisualStyleBackColor = true;
            this.btnRSAPublicBrowse.Click += new System.EventHandler(this.btnRSAPublicBrowse_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(8, 370);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(136, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "Ciphertext (in hexadecimal):";
            // 
            // tbRSACipherText
            // 
            this.tbRSACipherText.Location = new System.Drawing.Point(8, 389);
            this.tbRSACipherText.Multiline = true;
            this.tbRSACipherText.Name = "tbRSACipherText";
            this.tbRSACipherText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbRSACipherText.Size = new System.Drawing.Size(417, 133);
            this.tbRSACipherText.TabIndex = 16;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 193);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(50, 13);
            this.label17.TabIndex = 15;
            this.label17.Text = "Plaintext:";
            // 
            // tbRSAPlaintext
            // 
            this.tbRSAPlaintext.Location = new System.Drawing.Point(8, 212);
            this.tbRSAPlaintext.Multiline = true;
            this.tbRSAPlaintext.Name = "tbRSAPlaintext";
            this.tbRSAPlaintext.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbRSAPlaintext.Size = new System.Drawing.Size(417, 133);
            this.tbRSAPlaintext.TabIndex = 14;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 7);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(225, 13);
            this.label16.TabIndex = 4;
            this.label16.Text = "Select a folder where output files will be saved";
            // 
            // tbRSAOutputFileFolder
            // 
            this.tbRSAOutputFileFolder.Location = new System.Drawing.Point(6, 23);
            this.tbRSAOutputFileFolder.Name = "tbRSAOutputFileFolder";
            this.tbRSAOutputFileFolder.Size = new System.Drawing.Size(555, 20);
            this.tbRSAOutputFileFolder.TabIndex = 3;
            // 
            // btnBrowseFolder
            // 
            this.btnBrowseFolder.Location = new System.Drawing.Point(567, 21);
            this.btnBrowseFolder.Name = "btnBrowseFolder";
            this.btnBrowseFolder.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseFolder.TabIndex = 2;
            this.btnBrowseFolder.Text = "Browse";
            this.btnBrowseFolder.UseVisualStyleBackColor = true;
            this.btnBrowseFolder.Click += new System.EventHandler(this.btnBrowseFolder_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnGenerateRSAParameters);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.tbPrivKeyFileNameOut);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.tbPubKeyFileNameOut);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.cbKeySize);
            this.groupBox1.Location = new System.Drawing.Point(6, 62);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(855, 128);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Generate RSA parameters";
            // 
            // btnGenerateRSAParameters
            // 
            this.btnGenerateRSAParameters.Location = new System.Drawing.Point(5, 89);
            this.btnGenerateRSAParameters.Name = "btnGenerateRSAParameters";
            this.btnGenerateRSAParameters.Size = new System.Drawing.Size(75, 23);
            this.btnGenerateRSAParameters.TabIndex = 6;
            this.btnGenerateRSAParameters.Text = "Generate";
            this.btnGenerateRSAParameters.UseVisualStyleBackColor = true;
            this.btnGenerateRSAParameters.Click += new System.EventHandler(this.btnGenerateRSAParameters_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(282, 26);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(105, 13);
            this.label15.TabIndex = 5;
            this.label15.Text = "Private key file name";
            // 
            // tbPrivKeyFileNameOut
            // 
            this.tbPrivKeyFileNameOut.Location = new System.Drawing.Point(282, 45);
            this.tbPrivKeyFileNameOut.Name = "tbPrivKeyFileNameOut";
            this.tbPrivKeyFileNameOut.Size = new System.Drawing.Size(124, 20);
            this.tbPrivKeyFileNameOut.TabIndex = 4;
            this.tbPrivKeyFileNameOut.Text = "privKey.xml";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(144, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(101, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Public key file name";
            // 
            // tbPubKeyFileNameOut
            // 
            this.tbPubKeyFileNameOut.Location = new System.Drawing.Point(144, 45);
            this.tbPubKeyFileNameOut.Name = "tbPubKeyFileNameOut";
            this.tbPubKeyFileNameOut.Size = new System.Drawing.Size(124, 20);
            this.tbPubKeyFileNameOut.TabIndex = 2;
            this.tbPubKeyFileNameOut.Text = "pubKey.xml";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Select key size";
            // 
            // cbKeySize
            // 
            this.cbKeySize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbKeySize.Location = new System.Drawing.Point(5, 45);
            this.cbKeySize.Name = "cbKeySize";
            this.cbKeySize.Size = new System.Drawing.Size(121, 21);
            this.cbKeySize.TabIndex = 0;
            // 
            // tbAES
            // 
            this.tbAES.Controls.Add(this.groupBox3);
            this.tbAES.Controls.Add(this.groupBox2);
            this.tbAES.Controls.Add(this.btnGenerateKey);
            this.tbAES.Controls.Add(this.label25);
            this.tbAES.Controls.Add(this.tbAESKey);
            this.tbAES.Controls.Add(this.label24);
            this.tbAES.Controls.Add(this.cbAESPadding);
            this.tbAES.Controls.Add(this.label23);
            this.tbAES.Controls.Add(this.cbAESMode);
            this.tbAES.Controls.Add(this.label22);
            this.tbAES.Controls.Add(this.cbAESKeySize);
            this.tbAES.Controls.Add(this.label21);
            this.tbAES.Controls.Add(this.cbAESBlockSize);
            this.tbAES.Location = new System.Drawing.Point(4, 22);
            this.tbAES.Name = "tbAES";
            this.tbAES.Size = new System.Drawing.Size(867, 583);
            this.tbAES.TabIndex = 3;
            this.tbAES.Text = "AES";
            this.tbAES.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Controls.Add(this.btnAESParametersBrowse);
            this.groupBox3.Controls.Add(this.tbAESParametersIn);
            this.groupBox3.Controls.Add(this.btnAESDecrypt);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.tbAESDecryptedFileName);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.btnAESDecryptFileBrowse);
            this.groupBox3.Controls.Add(this.tbAESFileToDecrypt);
            this.groupBox3.Location = new System.Drawing.Point(19, 285);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(635, 168);
            this.groupBox3.TabIndex = 28;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Decryption";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 30);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(83, 13);
            this.label31.TabIndex = 31;
            this.label31.Text = "AES parameters";
            // 
            // btnAESParametersBrowse
            // 
            this.btnAESParametersBrowse.Location = new System.Drawing.Point(361, 49);
            this.btnAESParametersBrowse.Name = "btnAESParametersBrowse";
            this.btnAESParametersBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnAESParametersBrowse.TabIndex = 30;
            this.btnAESParametersBrowse.Text = "Browse";
            this.btnAESParametersBrowse.UseVisualStyleBackColor = true;
            this.btnAESParametersBrowse.Click += new System.EventHandler(this.btnAESParametersBrowse_Click);
            // 
            // tbAESParametersIn
            // 
            this.tbAESParametersIn.Location = new System.Drawing.Point(5, 49);
            this.tbAESParametersIn.Name = "tbAESParametersIn";
            this.tbAESParametersIn.Size = new System.Drawing.Size(350, 20);
            this.tbAESParametersIn.TabIndex = 29;
            // 
            // btnAESDecrypt
            // 
            this.btnAESDecrypt.Location = new System.Drawing.Point(9, 139);
            this.btnAESDecrypt.Name = "btnAESDecrypt";
            this.btnAESDecrypt.Size = new System.Drawing.Size(75, 23);
            this.btnAESDecrypt.TabIndex = 28;
            this.btnAESDecrypt.Text = "Decrypt";
            this.btnAESDecrypt.UseVisualStyleBackColor = true;
            this.btnAESDecrypt.Click += new System.EventHandler(this.btnAESDecrypt_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(457, 78);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(101, 13);
            this.label29.TabIndex = 27;
            this.label29.Text = "Decrypted file name";
            // 
            // tbAESDecryptedFileName
            // 
            this.tbAESDecryptedFileName.Location = new System.Drawing.Point(457, 97);
            this.tbAESDecryptedFileName.Name = "tbAESDecryptedFileName";
            this.tbAESDecryptedFileName.Size = new System.Drawing.Size(124, 20);
            this.tbAESDecryptedFileName.TabIndex = 26;
            this.tbAESDecryptedFileName.Text = "Decrypted.file";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 78);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(73, 13);
            this.label27.TabIndex = 26;
            this.label27.Text = "File to decrypt";
            // 
            // btnAESDecryptFileBrowse
            // 
            this.btnAESDecryptFileBrowse.Location = new System.Drawing.Point(361, 97);
            this.btnAESDecryptFileBrowse.Name = "btnAESDecryptFileBrowse";
            this.btnAESDecryptFileBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnAESDecryptFileBrowse.TabIndex = 25;
            this.btnAESDecryptFileBrowse.Text = "Browse";
            this.btnAESDecryptFileBrowse.UseVisualStyleBackColor = true;
            this.btnAESDecryptFileBrowse.Click += new System.EventHandler(this.btnAESDecryptFileBrowse_Click);
            // 
            // tbAESFileToDecrypt
            // 
            this.tbAESFileToDecrypt.Location = new System.Drawing.Point(5, 97);
            this.tbAESFileToDecrypt.Name = "tbAESFileToDecrypt";
            this.tbAESFileToDecrypt.Size = new System.Drawing.Size(350, 20);
            this.tbAESFileToDecrypt.TabIndex = 24;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.tbAESParametersOut);
            this.groupBox2.Controls.Add(this.btnAESEncrypt);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.tbAESEncryptedFileName);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.btnAESBrowseFileToEncrypt);
            this.groupBox2.Controls.Add(this.tbAESFileToEncrypt);
            this.groupBox2.Location = new System.Drawing.Point(19, 118);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(635, 139);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Encryption";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(457, 81);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(105, 13);
            this.label30.TabIndex = 28;
            this.label30.Text = "Parameters file name";
            // 
            // tbAESParametersOut
            // 
            this.tbAESParametersOut.Location = new System.Drawing.Point(457, 100);
            this.tbAESParametersOut.Name = "tbAESParametersOut";
            this.tbAESParametersOut.Size = new System.Drawing.Size(124, 20);
            this.tbAESParametersOut.TabIndex = 27;
            this.tbAESParametersOut.Text = "Parameters.out";
            // 
            // btnAESEncrypt
            // 
            this.btnAESEncrypt.Location = new System.Drawing.Point(9, 110);
            this.btnAESEncrypt.Name = "btnAESEncrypt";
            this.btnAESEncrypt.Size = new System.Drawing.Size(75, 23);
            this.btnAESEncrypt.TabIndex = 26;
            this.btnAESEncrypt.Text = "Encrypt";
            this.btnAESEncrypt.UseVisualStyleBackColor = true;
            this.btnAESEncrypt.Click += new System.EventHandler(this.btnAESEncrypt_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(457, 26);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(100, 13);
            this.label28.TabIndex = 25;
            this.label28.Text = "Encrypted file name";
            // 
            // tbAESEncryptedFileName
            // 
            this.tbAESEncryptedFileName.Location = new System.Drawing.Point(457, 45);
            this.tbAESEncryptedFileName.Name = "tbAESEncryptedFileName";
            this.tbAESEncryptedFileName.Size = new System.Drawing.Size(124, 20);
            this.tbAESEncryptedFileName.TabIndex = 24;
            this.tbAESEncryptedFileName.Text = "Encrypted.file";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 26);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(73, 13);
            this.label26.TabIndex = 23;
            this.label26.Text = "File to encrypt";
            // 
            // btnAESBrowseFileToEncrypt
            // 
            this.btnAESBrowseFileToEncrypt.Location = new System.Drawing.Point(361, 45);
            this.btnAESBrowseFileToEncrypt.Name = "btnAESBrowseFileToEncrypt";
            this.btnAESBrowseFileToEncrypt.Size = new System.Drawing.Size(75, 23);
            this.btnAESBrowseFileToEncrypt.TabIndex = 22;
            this.btnAESBrowseFileToEncrypt.Text = "Browse";
            this.btnAESBrowseFileToEncrypt.UseVisualStyleBackColor = true;
            this.btnAESBrowseFileToEncrypt.Click += new System.EventHandler(this.btnAESBrowseFileToEncrypt_Click);
            // 
            // tbAESFileToEncrypt
            // 
            this.tbAESFileToEncrypt.Location = new System.Drawing.Point(5, 45);
            this.tbAESFileToEncrypt.Name = "tbAESFileToEncrypt";
            this.tbAESFileToEncrypt.Size = new System.Drawing.Size(350, 20);
            this.tbAESFileToEncrypt.TabIndex = 21;
            // 
            // btnGenerateKey
            // 
            this.btnGenerateKey.Location = new System.Drawing.Point(564, 76);
            this.btnGenerateKey.Name = "btnGenerateKey";
            this.btnGenerateKey.Size = new System.Drawing.Size(90, 23);
            this.btnGenerateKey.TabIndex = 10;
            this.btnGenerateKey.Text = "Generate Key";
            this.btnGenerateKey.UseVisualStyleBackColor = true;
            this.btnGenerateKey.Click += new System.EventHandler(this.btnGenerateKey_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(19, 81);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(96, 13);
            this.label25.TabIndex = 9;
            this.label25.Text = "Key (hexadecimal):";
            // 
            // tbAESKey
            // 
            this.tbAESKey.Location = new System.Drawing.Point(121, 78);
            this.tbAESKey.Name = "tbAESKey";
            this.tbAESKey.Size = new System.Drawing.Size(437, 20);
            this.tbAESKey.TabIndex = 8;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(434, 19);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(46, 13);
            this.label24.TabIndex = 7;
            this.label24.Text = "Padding";
            // 
            // cbAESPadding
            // 
            this.cbAESPadding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAESPadding.FormattingEnabled = true;
            this.cbAESPadding.Location = new System.Drawing.Point(437, 35);
            this.cbAESPadding.Name = "cbAESPadding";
            this.cbAESPadding.Size = new System.Drawing.Size(121, 21);
            this.cbAESPadding.TabIndex = 6;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(297, 19);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(34, 13);
            this.label23.TabIndex = 5;
            this.label23.Text = "Mode";
            // 
            // cbAESMode
            // 
            this.cbAESMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAESMode.FormattingEnabled = true;
            this.cbAESMode.Location = new System.Drawing.Point(300, 35);
            this.cbAESMode.Name = "cbAESMode";
            this.cbAESMode.Size = new System.Drawing.Size(121, 21);
            this.cbAESMode.TabIndex = 4;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(155, 19);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(46, 13);
            this.label22.TabIndex = 3;
            this.label22.Text = "Key size";
            // 
            // cbAESKeySize
            // 
            this.cbAESKeySize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAESKeySize.FormattingEnabled = true;
            this.cbAESKeySize.Location = new System.Drawing.Point(158, 35);
            this.cbAESKeySize.Name = "cbAESKeySize";
            this.cbAESKeySize.Size = new System.Drawing.Size(121, 21);
            this.cbAESKeySize.TabIndex = 2;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(16, 19);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(55, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Block size";
            // 
            // cbAESBlockSize
            // 
            this.cbAESBlockSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAESBlockSize.FormattingEnabled = true;
            this.cbAESBlockSize.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbAESBlockSize.Location = new System.Drawing.Point(19, 35);
            this.cbAESBlockSize.Name = "cbAESBlockSize";
            this.cbAESBlockSize.Size = new System.Drawing.Size(121, 21);
            this.cbAESBlockSize.TabIndex = 0;
            // 
            // tbRabinMiller
            // 
            this.tbRabinMiller.Controls.Add(this.tbAnswers);
            this.tbRabinMiller.Controls.Add(this.label3);
            this.tbRabinMiller.Controls.Add(this.nudA);
            this.tbRabinMiller.Controls.Add(this.btnTestRabinMiller);
            this.tbRabinMiller.Controls.Add(this.label2);
            this.tbRabinMiller.Controls.Add(this.label1);
            this.tbRabinMiller.Controls.Add(this.tbRabinMillerProbablePrime);
            this.tbRabinMiller.Controls.Add(this.nudRabinMillerK);
            this.tbRabinMiller.Location = new System.Drawing.Point(4, 22);
            this.tbRabinMiller.Name = "tbRabinMiller";
            this.tbRabinMiller.Padding = new System.Windows.Forms.Padding(3);
            this.tbRabinMiller.Size = new System.Drawing.Size(867, 583);
            this.tbRabinMiller.TabIndex = 0;
            this.tbRabinMiller.Text = "Rabin-Miller";
            this.tbRabinMiller.UseVisualStyleBackColor = true;
            // 
            // tbAnswers
            // 
            this.tbAnswers.Location = new System.Drawing.Point(20, 116);
            this.tbAnswers.Multiline = true;
            this.tbAnswers.Name = "tbAnswers";
            this.tbAnswers.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbAnswers.Size = new System.Drawing.Size(585, 421);
            this.tbAnswers.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(482, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 28);
            this.label3.TabIndex = 6;
            this.label3.Text = "Specific \'a\' value (optional. Leave \'1\' if you do not want to use any specified \'" +
    "a\')";
            // 
            // nudA
            // 
            this.nudA.Location = new System.Drawing.Point(485, 43);
            this.nudA.Maximum = new decimal(new int[] {
            -1,
            2147483647,
            0,
            0});
            this.nudA.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudA.Name = "nudA";
            this.nudA.Size = new System.Drawing.Size(120, 20);
            this.nudA.TabIndex = 5;
            this.nudA.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnTestRabinMiller
            // 
            this.btnTestRabinMiller.Location = new System.Drawing.Point(20, 68);
            this.btnTestRabinMiller.Name = "btnTestRabinMiller";
            this.btnTestRabinMiller.Size = new System.Drawing.Size(75, 23);
            this.btnTestRabinMiller.TabIndex = 4;
            this.btnTestRabinMiller.Text = "Test";
            this.btnTestRabinMiller.UseVisualStyleBackColor = true;
            this.btnTestRabinMiller.Click += new System.EventHandler(this.btnTestRabinMiller_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(331, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Times to test";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "A probable prime number to test";
            // 
            // tbRabinMillerProbablePrime
            // 
            this.tbRabinMillerProbablePrime.Location = new System.Drawing.Point(20, 42);
            this.tbRabinMillerProbablePrime.Name = "tbRabinMillerProbablePrime";
            this.tbRabinMillerProbablePrime.Size = new System.Drawing.Size(305, 20);
            this.tbRabinMillerProbablePrime.TabIndex = 1;
            // 
            // nudRabinMillerK
            // 
            this.nudRabinMillerK.Location = new System.Drawing.Point(334, 42);
            this.nudRabinMillerK.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.nudRabinMillerK.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudRabinMillerK.Name = "nudRabinMillerK";
            this.nudRabinMillerK.Size = new System.Drawing.Size(120, 20);
            this.nudRabinMillerK.TabIndex = 0;
            this.nudRabinMillerK.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label32
            // 
            this.label32.Location = new System.Drawing.Point(444, 32);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(417, 35);
            this.label32.TabIndex = 31;
            this.label32.Text = "If two plaintext messages were encrypted with the same key (using stream cipher) " +
    "and only one plaintext is known, the other (unknown) plaintext can be obtained.";
            // 
            // Information_Security_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 633);
            this.Controls.Add(this.tcAll);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Information_Security_Main";
            this.Text = "Information Security";
            this.tcAll.ResumeLayout(false);
            this.tbRC4.ResumeLayout(false);
            this.tbRC4.PerformLayout();
            this.tbRSA.ResumeLayout(false);
            this.tbRSA.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tbAES.ResumeLayout(false);
            this.tbAES.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tbRabinMiller.ResumeLayout(false);
            this.tbRabinMiller.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRabinMillerK)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcAll;
        private System.Windows.Forms.TabPage tbRabinMiller;
        private System.Windows.Forms.TabPage tbRC4;
        private System.Windows.Forms.TabPage tbRSA;
        private System.Windows.Forms.Button btnTestRabinMiller;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbRabinMillerProbablePrime;
        private System.Windows.Forms.NumericUpDown nudRabinMillerK;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudA;
        private System.Windows.Forms.TextBox tbAnswers;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbKPAUnknownPT;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnCalculateUnknownPlaintext;
        private System.Windows.Forms.TextBox tbKPAKnownCTofUnknownPT;
        private System.Windows.Forms.TextBox tbKPAKnownCTofKnownPT;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbKPAKnownPT;
        private System.Windows.Forms.Button btnDecryptRC4;
        private System.Windows.Forms.Button btnEncryptRC4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbKeyRC4;
        private System.Windows.Forms.TextBox tbRC4Ciphertext;
        private System.Windows.Forms.TextBox tbRC4Plaintext;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbKeySize;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbRSAOutputFileFolder;
        private System.Windows.Forms.Button btnBrowseFolder;
        private System.Windows.Forms.Button btnGenerateRSAParameters;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tbPrivKeyFileNameOut;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbPubKeyFileNameOut;
        private System.Windows.Forms.Button btnRSADecrypt;
        private System.Windows.Forms.Button btnRSAEncrypt;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbRSAPrivateBrowse;
        private System.Windows.Forms.Button btnRSAPrivateBrowse;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbRSAPublicBrowse;
        private System.Windows.Forms.Button btnRSAPublicBrowse;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbRSACipherText;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbRSAPlaintext;
        private System.Windows.Forms.CheckBox cbIsHexadecimal;
        private System.Windows.Forms.CheckBox cbDecryptToHexadecimal;
        private System.Windows.Forms.TabPage tbAES;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button btnAESParametersBrowse;
        private System.Windows.Forms.TextBox tbAESParametersIn;
        private System.Windows.Forms.Button btnAESDecrypt;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tbAESDecryptedFileName;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btnAESDecryptFileBrowse;
        private System.Windows.Forms.TextBox tbAESFileToDecrypt;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox tbAESParametersOut;
        private System.Windows.Forms.Button btnAESEncrypt;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tbAESEncryptedFileName;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btnAESBrowseFileToEncrypt;
        private System.Windows.Forms.TextBox tbAESFileToEncrypt;
        private System.Windows.Forms.Button btnGenerateKey;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tbAESKey;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cbAESPadding;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cbAESMode;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cbAESKeySize;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cbAESBlockSize;
        private System.Windows.Forms.Label label32;
    }
}

