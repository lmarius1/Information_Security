﻿using AESLib;
using ConversionsLib;
using RabinMiller;
using RC4Lib;
using RSALib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Information_Security
{
    public partial class Information_Security_Main : Form
    {
        private Conversions conv = Conversions.getInstance();
        private RabinMillerTest rmt;
        private RC4 rc4;
        private string path = Path.GetFullPath(Path.Combine(System.Reflection.Assembly.GetExecutingAssembly().Location, "..\\"));
        private AESC aes = new AESC();

        public Information_Security_Main()
        {
            InitializeComponent();
            rmt = new RabinMillerTest(Print);
            rc4 = new RC4();

            setRSAKeySizeItems();
            tbRSAOutputFileFolder.Text = path;

            setAESBlockSize();
            setAESKeySize();
            setAESCipherModeCB();
            setAESPadding();
        }

        private void setAESCipherModeCB()
        {
            List<CipherMode> list = new List<CipherMode>
            {
                CipherMode.CBC, CipherMode.ECB
            };
            cbAESMode.DataSource = list;
        }

        private void setAESPadding()
        {
            List<PaddingMode> list = new List<PaddingMode>
            {
                PaddingMode.PKCS7, PaddingMode.ANSIX923, PaddingMode.ISO10126,
                PaddingMode.Zeros, PaddingMode.None
            };
            cbAESPadding.DataSource = list;
        }

        private void setAESKeySize()
        {
            List<int> list = new List<int>
            {
                128, 192, 256
            };
            cbAESKeySize.DataSource = list;
            cbAESKeySize.SelectedIndex = 0;
        }

        private void setAESBlockSize()
        {
            List<int> list = new List<int>
            {
                128
            };
            cbAESBlockSize.DataSource = list;
            cbAESBlockSize.SelectedIndex = 0;
        }

        private void setRSAKeySizeItems()
        {
            List<string> list = new List<string>
            {
                "384", "512", "1024", "2048", "3072", "4096"
            };
            cbKeySize.DataSource = list;
            cbKeySize.SelectedIndex = 3;
        }

        public void Print(string s)
        {
            tbAnswers.Text += Environment.NewLine + s;
        }

        private void btnTestRabinMiller_Click(object sender, EventArgs e)
        {
            tbAnswers.Text = "";
            BigInteger source;
            BigInteger.TryParse(tbRabinMillerProbablePrime.Text, out source);
            if (source == null || source <= 0)
            {
                MessageBox.Show("Wrong input value: " + tbRabinMillerProbablePrime.Text +
                    Environment.NewLine + "The number should be a positive integer.");
                return;
            }
            int count = (int)nudRabinMillerK.Value;

            BigInteger a = BigInteger.Parse(nudA.Value.ToString());
            if (source > 1 && a > source - 1)
            {
                MessageBox.Show("a should satisfy: 1 < a < n-1");
                return;
            }


            if (rmt.IsProbablePrime(source, count, a))
            {
                MessageBox.Show(source + " is probably a prime.");
            } else
            {
                MessageBox.Show(source + " is certainly not a prime.");
            }

        }

        private void btnEncryptRC4_Click(object sender, EventArgs e)
        {
            string key = tbKeyRC4.Text;
            string plainText = tbRC4Plaintext.Text;

            if (string.IsNullOrEmpty(key))
            {
                MessageBox.Show("key cannot be empty.");
                return;
            }

            if (string.IsNullOrEmpty(plainText))
            {
                MessageBox.Show("plaintext cannot be empty.");
            }

            try
            {
                string cipherText = conv.ByteArrayToHexString(rc4.Encrypt(key, plainText));
                tbRC4Ciphertext.Text = cipherText;
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error :(\nError message: " + exc.Message);
            }
        }

        private void btnDecryptRC4_Click(object sender, EventArgs e)
        {
            string key = tbKeyRC4.Text;
            string cipherText = tbRC4Ciphertext.Text.ToLower();

            if (string.IsNullOrEmpty(key))
            {
                MessageBox.Show("key cannot be empty.");
                return;
            }

            if (string.IsNullOrEmpty(cipherText))
            {
                MessageBox.Show("cipherText cannot be empty.");
            }

            try
            {
                string plainText = conv.ByteArrayToASCIIText(
                    rc4.Decrypt(conv.StringToByteArray(key), conv.HexStringToByteArray(cipherText)));

                tbRC4Plaintext.Text = plainText;
            } catch (Exception exc)
            {
                MessageBox.Show("Error :(\nError message: " + exc.Message);
            }
        }

        private void btnCalculateUnknownPlaintext_Click(object sender, EventArgs e)
        {
            string pt1 = tbKPAKnownPT.Text;
            string ct1 = tbKPAKnownCTofKnownPT.Text.ToLower();
            string ct2 = tbKPAKnownCTofUnknownPT.Text.ToLower();

            if (string.IsNullOrEmpty(pt1))
            {
                MessageBox.Show("Known plaintext cannot be empty.\n");
                return;
            }
            if (string.IsNullOrEmpty(ct1))
            {
                MessageBox.Show("Known Ciphertext of the known Plaintext cannot be empty.");
                return;
            }
            if (string.IsNullOrEmpty(ct2))
            {
                MessageBox.Show("Known Ciphertext of the unknown Plaintext cannot be empty.");
                return;
            }

            try
            {
                //Length of ct1 and ct2 must be equal in order to calculate unknown plaintext.
                //If ct1.Length > ct2.Length then only part of the pt2 will be obtained.
                string pt2 = conv.ByteArrayToASCIIText(rc4.DecryptOtherPlaintext(conv.StringToByteArray(pt1),
                    conv.HexStringToByteArray(ct1), conv.HexStringToByteArray(ct2)));

                tbKPAUnknownPT.Text = pt2;
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error :(\nError message: " + exc.Message);
            }
        }

        private void btnBrowseFolder_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                if (Directory.Exists(tbRSAOutputFileFolder.Text))
                    fbd.SelectedPath = tbRSAOutputFileFolder.Text;
                else if (Directory.Exists(path))
                    fbd.SelectedPath = path;

                fbd.RootFolder = Environment.SpecialFolder.MyComputer;
                fbd.ShowNewFolderButton = false;     // use if appropriate in your application
                SendKeys.Send("{TAB}{TAB}{RIGHT}");  // <<-- Workaround

                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    path = fbd.SelectedPath;
                    tbRSAOutputFileFolder.Text = fbd.SelectedPath;
                }
            }
        }

        #region RSA

        private int rsaLength;
        private string rsaOutPath;
        private string rsaPubKeyFileNameOut;
        private string rsaPrivKeyFileNameOut;

        private void getParameterValuesForRSAGenerationFromForm()
        {
            rsaLength = Convert.ToInt32(cbKeySize.SelectedValue.ToString());
            rsaOutPath = tbRSAOutputFileFolder.Text;

            if (!Directory.Exists(rsaOutPath))
            {
                MessageBox.Show("Invalid directory: " + rsaOutPath);
                return;
            }

            rsaPubKeyFileNameOut = tbPubKeyFileNameOut.Text;
            rsaPrivKeyFileNameOut = tbPrivKeyFileNameOut.Text;
        }


        private void generateRSAParameters()
        {
            try
            {
                RSAGenerator rsaGenerator = new RSAGenerator(rsaLength);

                File.WriteAllText(Path.Combine(rsaOutPath, rsaPubKeyFileNameOut), rsaGenerator.PubKeyString);
                File.WriteAllText(Path.Combine(rsaOutPath, rsaPrivKeyFileNameOut), rsaGenerator.PrivKeyString);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error :(\nError message: " + exc.Message);
            }
        }

        private void btnGenerateRSAParameters_Click(object sender, EventArgs e)
        {
            //read all values from this form (Form1) that are necessary for rsa generation.
            getParameterValuesForRSAGenerationFromForm();

            using (FrmWaitForm frm = new FrmWaitForm(generateRSAParameters))
            {
                frm.ShowDialog(this);
            }
        }

        private void btnRSAPublicBrowse_Click(object sender, EventArgs e)
        {
            showFileDialog(tbRSAPublicBrowse);
        }

        private void btnRSAPrivateBrowse_Click(object sender, EventArgs e)
        {
            showFileDialog(tbRSAPrivateBrowse);
        }

        private void showFileDialog(TextBox textBox)
        {
            try
            {
                using (FileDialog FD = new OpenFileDialog())
                {
                    if (Directory.Exists(textBox.Text))
                        FD.InitialDirectory = textBox.Text;
                    else if (Directory.Exists(path))
                        FD.InitialDirectory = path;

                    if (FD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        textBox.Text = FD.FileName;
                        path = FD.FileName;
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error :(\nError message: " + exc.Message);
            }
        }

        private void btnRSAEncrypt_Click(object sender, EventArgs e)
        {
            if (!File.Exists(tbRSAPublicBrowse.Text))
            {
                MessageBox.Show("File: " + tbRSAPublicBrowse.Text + " does not exist.");
                return;
            }
            try
            {
                string pubKey = File.ReadAllText(tbRSAPublicBrowse.Text);
                string pt = tbRSAPlaintext.Text;
                RSAEnc rsa = new RSAEnc(pubKey);
                string cipherText = rsa.Encrypt(pt, cbIsHexadecimal.Checked);
                tbRSACipherText.Text = cipherText;
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error :(\nError message: " + exc.Message);
            }
        }

        private void btnRSADecrypt_Click(object sender, EventArgs e)
        {
            if (!File.Exists(tbRSAPrivateBrowse.Text))
            {
                MessageBox.Show("File: " + tbRSAPrivateBrowse.Text + " does not exist.");
                return;
            }
            try
            {
                string privKey = File.ReadAllText(tbRSAPrivateBrowse.Text);
                string ct = tbRSACipherText.Text;
                RSADec rsaDec = new RSADec(privKey);
                string plainText = rsaDec.Decrypt(ct, cbDecryptToHexadecimal.Checked);
                tbRSAPlaintext.Text = plainText;
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error :(\nError message: " + exc.Message);
            }
        }
        #endregion

        #region AES

        private bool updateAESAlgParameters()
        {
            bool changesWereMade = false;
            int blockSize = (int)cbAESBlockSize.SelectedValue;
            int keySize = (int)cbAESKeySize.SelectedValue;
            CipherMode cipherMode = (CipherMode)cbAESMode.SelectedValue;
            PaddingMode paddingMode = (PaddingMode)cbAESPadding.SelectedValue;

            if (aes.aesAlg.KeySize != keySize)
            {
                aes.aesAlg.KeySize = keySize;
                changesWereMade = true;
            }
            if (aes.aesAlg.BlockSize != blockSize)
            {
                aes.aesAlg.BlockSize = blockSize;
                changesWereMade = true;
            }
            if (aes.aesAlg.Mode != cipherMode)
            {
                aes.aesAlg.Mode = cipherMode;
                changesWereMade = true;
            }
            if (aes.aesAlg.Padding != paddingMode)
            {
                aes.aesAlg.Padding = paddingMode;
                changesWereMade = true;
            }
            return changesWereMade;
        }

        private void btnGenerateKey_Click(object sender, EventArgs e)
        {
            updateAESAlgParameters();
            aes.aesAlg.GenerateKey();
            byte[] keyInBytes = aes.aesAlg.Key;
            string hexKey = conv.ByteArrayToHexString(keyInBytes);
            tbAESKey.Text = hexKey;
        }

        private void btnAESBrowseFileToEncrypt_Click(object sender, EventArgs e)
        {
            showFileDialog(tbAESFileToEncrypt);
        }

        private void btnAESDecryptFileBrowse_Click(object sender, EventArgs e)
        {
            showFileDialog(tbAESFileToDecrypt);
        }

        private void btnAESParametersBrowse_Click(object sender, EventArgs e)
        {
            showFileDialog(tbAESParametersIn);
        }



        private string aesFileToEncrypt;
        private string aesEncryptedFileName;
        private string aesParametersOutFileName;


        private void aesEncrypt()
        {
            try
            {
                string encryptedFileName = Path.Combine(Path.GetDirectoryName(aesFileToEncrypt), aesEncryptedFileName);

                using (FileStream Fin = new FileStream(aesFileToEncrypt, FileMode.Open, FileAccess.Read))
                using (FileStream Fout = new FileStream(encryptedFileName, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    Fout.SetLength(0);

                    aes.Encrypt(Fin, Fout);

                    string aesParametersFileName = Path.Combine(Path.GetDirectoryName(aesFileToEncrypt), aesParametersOutFileName);
                    File.WriteAllText(aesParametersFileName, aes.exportAESParameters());
                    MessageBox.Show("Encryption completed.");
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error :(\nError message: " + exc.Message);
            }
        }

        private bool getParameterValuesForAESEncryptionAndGenerateAESParamsIfNecessary()
        {
            bool parametersChanged = updateAESAlgParameters();

            if (!parametersChanged && !string.IsNullOrEmpty(tbAESKey.Text))
            {
                byte[] aesKey = conv.HexStringToByteArray(tbAESKey.Text);
                aes.aesAlg.Key = aesKey;
            }
            else
            {
                aes.aesAlg.GenerateKey();
                byte[] keyInBytes = aes.aesAlg.Key;
                string hexKey = conv.ByteArrayToHexString(keyInBytes);
                tbAESKey.Text = hexKey;
            }

            aesFileToEncrypt = tbAESFileToEncrypt.Text;
            if (!File.Exists(aesFileToEncrypt))
            {
                MessageBox.Show("File: " + aesFileToEncrypt + " does not exist.");
                return false;
            }

            aesEncryptedFileName = tbAESEncryptedFileName.Text;
            aesParametersOutFileName = tbAESParametersOut.Text;
            return true;
        }

        private void btnAESEncrypt_Click(object sender, EventArgs e)
        {
            if (!getParameterValuesForAESEncryptionAndGenerateAESParamsIfNecessary())
                return;

            using (FrmWaitForm frm = new FrmWaitForm(aesEncrypt))
            {
                frm.ShowDialog(this);
            }
        }

        private string aesParametersFile;
        private string aesFileToDecrypt;
        private string aesKey;
        private string aesDecryptedFile;

        private bool getParameterValuesForAESDecryption()
        {
            aesParametersFile = tbAESParametersIn.Text;
            if (!File.Exists(aesParametersFile))
            {
                MessageBox.Show("AES parameters file not found.");
                return false;
            }
            aesFileToDecrypt = tbAESFileToDecrypt.Text;
            if (!File.Exists(aesFileToDecrypt))
            {
                MessageBox.Show("No file to decrypt chosen.");
                return false;
            }
            aesKey = tbAESKey.Text;
            if (string.IsNullOrEmpty(aesKey))
            {
                MessageBox.Show("AES key cannot be empty.");
                return false;
            }

            aesDecryptedFile = Path.Combine(Path.GetDirectoryName(aesFileToDecrypt), tbAESDecryptedFileName.Text);
            return true;
        }

        private void aesDecrypt()
        {
            try
            {
                byte[] key;
                try
                {
                    key = conv.HexStringToByteArray(aesKey);
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Error occured while converting hexadecimal key.\nMessage: " + exc.Message);
                    return;
                }

                try
                {
                    string[] aesParameters = File.ReadAllLines(aesParametersFile);
                    aes.importAESParameters(aesParameters);
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Error occured while reading AES parameters.\nMessage: " + exc.Message);
                    return;
                }

                using (FileStream Fin = new FileStream(aesFileToDecrypt, FileMode.Open, FileAccess.Read))
                using (FileStream Fout = new FileStream(aesDecryptedFile, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    Fout.SetLength(0);
                    aes.Decrypt(Fin, Fout, key, 16);    //offset is 16 because first 16 bytes are for IV (used both in encryption and decryption)

                    MessageBox.Show("Decryption completed.");
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error :(\nError message: " + exc.Message);
            }
        }

        private void btnAESDecrypt_Click(object sender, EventArgs e)
        {
            if (!getParameterValuesForAESDecryption())
                return;

            using (FrmWaitForm frm = new FrmWaitForm(aesDecrypt))
            {
                frm.ShowDialog(this);
            }
        }
        #endregion
    }
}
