﻿using ConversionsLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RC4Lib
{
    public class RC4
    {
        private Conversions conv = Conversions.getInstance();
        #region Public
        /// <summary>
        /// encrypts plain text based on a key and returns cipher text in hexadecimal form or otherwise
        /// returns in plain ASCII text (if corresponding bool argument is false).
        /// </summary>
        /// <param name="hexKey"></param>
        /// <param name="hexPlainText"></param>
        /// <param name="returnInHexStringForm"></param>
        /// <returns></returns>
        public string Encrypt(string hexKey, string hexPlainText, bool returnInHexStringForm)
        {
            
            byte[] s = GetS(conv.HexStringToByteArray(hexKey));
            byte[] plainText = conv.HexStringToByteArray(hexPlainText);
            byte[] h = GetH(s, plainText.Length);
            byte[] answ = conv.XOR(plainText, h);
            if (returnInHexStringForm)
                return conv.ByteArrayToHexString(answ);
            return conv.ByteArrayToASCIIText(answ);
        }

        /// <summary>
        /// deprypts and returns decrypted text in hexadecimal form (or if false, then in plain ASCII text form).
        /// </summary>
        /// <param name="hexKey"></param>
        /// <param name="hexCipherText"></param>
        /// <param name="returnInHexStringForm"></param>
        /// <returns></returns>
        public string Decrypt(string hexKey, string hexCipherText, bool returnInHexStringForm)
        {
            byte[] s = GetS(conv.HexStringToByteArray(hexKey));
            byte[] plainText = conv.HexStringToByteArray(hexCipherText);
            byte[] h = GetH(s, plainText.Length);
            byte[] answ = conv.XOR(plainText, h);
            if (returnInHexStringForm)
                return conv.ByteArrayToHexString(answ);
            return conv.ByteArrayToASCIIText(answ);
        }
        public byte[] Encrypt(string ASCIIKey, string ASCIIPlainText)
        {
            return Encrypt(conv.StringToByteArray(ASCIIKey), conv.StringToByteArray(ASCIIPlainText));
        }
        public byte[] Encrypt(byte[] key, byte[] plainText)
        {
            byte[] s = GetS(key);
            byte[] h = GetH(s, plainText.Length);
            byte[] answ = conv.XOR(plainText, h);
            return answ;
        }
        public byte[] Decrypt(byte[] key, byte[] cipherText)
        {
            byte[] s = GetS(key);
            byte[] h = GetH(s, cipherText.Length);
            byte[] answ = conv.XOR(cipherText, h);
            return answ;
        }
        public byte[] DecryptOtherPlaintext(byte[] pt1, byte[] ct1, byte[] ct2)
        {
            //Length of ct1 and ct2 must be equal in order to calculate unknown plaintext.
            //If ct1.Length < ct2.Length then only part of the pt2 will be obtained.
            if (ct1.Length < ct2.Length)
            {
                //Make ct2 shorter
                ct2 = SubArray(ct2, 0, ct1.Length);
            }
            else if (ct1.Length > ct2.Length)
            {
                //make ct1 and pt1 shorter
                ct1 = SubArray(ct1, 0, ct2.Length);
                pt1 = SubArray(pt1, 0, ct2.Length);
            }

            //just XOR'ing all three byte arrays will reveal the pt2.
            byte[] res = new byte[pt1.Length];
            pt1.CopyTo(res, 0);
            for (int i = 0; i < ct1.Length; i++)
            {
                res[i] ^= ct1[i];
                res[i] ^= ct2[i];
            }
            return res;
        }
        public byte[] DecryptOtherPlaintext(string hexPT1, string hexCT1, string hexCT2)
        {
            return DecryptOtherPlaintext(conv.HexStringToByteArray(hexPT1), conv.HexStringToByteArray(hexCT1), 
                conv.HexStringToByteArray(hexCT2));
        }
        #endregion
        #region Private
        private byte[] GetS(byte[] key)
        {
            if (key.Length == 0)
                throw new Exception("Key length cannot be zero.");
            byte[] s = new byte[256];
            for (int i = 0; i < 256; i++)
            {
                s[i] = (byte)i;
            }
            int n = key.Length;
            int j = 0;
            for (int i = 0; i < 256; i++)
            {
                j = (j + s[i] + (key[i % n])) % 256;
                conv.Swap(s, i, j);
            }
            return s;
        }
        private byte[] GetH(byte[] s, int n)
        {
            byte[] h = new byte[n];
            int i = 0, j = 0;
            for (int index = 0; index < n; index++)
            {
                i = (i + 1) % 256;
                j = (j + s[i]) % 256;
                conv.Swap(s, i, j);
                h[index] = s[(s[i] + s[j]) % 256];
            }
            return h;
        }

        private T[] SubArray<T>(T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }
        #endregion
    }
}
