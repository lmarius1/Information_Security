﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Rabin-Miller primality test.
/// Code used from: http://rosettacode.org/wiki/Miller%E2%80%93Rabin_primality_test#C.23
/// </summary>

namespace RabinMiller
{
    // Miller-Rabin primality test as an extension method on the BigInteger type.
    // Based on the Ruby implementation on this page.
    public class RabinMillerTest
    {
        public delegate void Print(string s);
        private Print printDelegate;

        public RabinMillerTest(Print printDelegate)
        {
            this.printDelegate = printDelegate;
        }


        public RabinMillerTest()
        {

        }

        /// <summary>
        /// </summary>
        /// <param name="source">Source is the possibly prime number to be checked.</param>
        /// <param name="certainty">Certainty is the number of times to check.</param>
        /// <param name="a">a is a value that source will be tested with. If a <= 1 then it will be interpreted as null and not taken into consideration.</param>
        /// <returns>False means that source certainly is not a prime. True means that source is probably a prime</returns>
        public bool IsProbablePrime(BigInteger source, int certainty, BigInteger a)
        {
            if (source == 2 || source == 3)
                return true;
            if (source < 2 || source % 2 == 0)
                return false;

            BigInteger d = source - 1;
            int s = 0;

            while (d % 2 == 0)
            {
                d /= 2;
                s += 1;
            }

            //If a is specified then return the Rabin-Miller test result by only testing with that a.
            if (a > 1)
            {
                return PerformTestWithA(a, d, source, s);
            }

            // There is no built-in method for generating random BigInteger values.
            // Instead, random BigIntegers are constructed from randomly generated
            // byte arrays of the same length as the source.
            RandomNumberGenerator rng = RandomNumberGenerator.Create();
            byte[] bytes = new byte[source.ToByteArray().LongLength];

            for (int i = 0; i < certainty; i++)
            {
                a = GenerateA(bytes, rng, source);

                if (!PerformTestWithA(a, d, source, s))
                {
                    PrintIfInterfaceNotNull(false, a, source);
                    return false;
                } else
                {
                    PrintIfInterfaceNotNull(true, a, source);
                }
            }
            return true;
        }

        private void PrintIfInterfaceNotNull(bool isProbablyPrime, BigInteger a, BigInteger source)
        {
            if (printDelegate != null)
            {
                if (isProbablyPrime)
                {
                    printDelegate(a + " says: " + source + " is Probably prime.");
                }
                else
                {
                    printDelegate(a + " says: " + source + " is certainly not a prime.");
                }
            }
        }

        private bool PerformTestWithA(BigInteger a, BigInteger d, BigInteger source, BigInteger s)
        {
            BigInteger x = BigInteger.ModPow(a, d, source);

            //If first x is equal to 1 or -1 mod p, then a says that it is probably prime.
            if (x == 1 || x == source - 1)
                return true;

            //In later attempts if x is equal to 1, then a is a witness that source is not a prime.
            //If x is -1, then it says that source is probably a prime.
            for (int r = 1; r < s; r++)
            {
                x = BigInteger.ModPow(x, 2, source);
                if (x == 1)
                    return false;
                if (x == source - 1)
                    return true;
            }

            return false;
        }

        private BigInteger GenerateA(byte[] bytes, RandomNumberGenerator rng, BigInteger source)
        {
            BigInteger a;
            do
            {
                // This may raise an exception in Mono 2.10.8 and earlier.
                // http://bugzilla.xamarin.com/show_bug.cgi?id=2761
                rng.GetBytes(bytes);
                a = new BigInteger(bytes);

                //Manually edited part by me so that that integers are not generated again (except when value is 0 or +-1)
                if (a < -1)
                {
                    a *= -1;
                }
                if (a >= source - 2)
                {
                    a %= source - 2;
                }
            }
            while (a < 2);

            return a;
        }
    }
}
